obj/main.o: src/main.cpp
	g++ $(shell pkg-config --cflags libpq) -o obj/main.o -c src/main.cpp

obj/affichage.o: src/affichage.cpp
	g++ $(shell pkg-config --cflags libpq) -o obj/affichage.o -c src/affichage.cpp

obj/calcul.o: src/calcul.cpp
	g++ $(shell pkg-config --cflags libpq) -o obj/calcul.o -c src/calcul.cpp

bin/affichage_table: obj/main.o obj/affichage.o obj/calcul.o
	g++ $(shell pkg-config --libs libpq) -o bin/affichage_table obj/main.o obj/affichage.o obj/calcul.o
add:
	git add *

clean:
	-rm obj/*.o bin/*

